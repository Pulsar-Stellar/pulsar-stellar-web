from django.urls import path
from . import views

urlpatterns = [
    path('logout/', views.verify_logout, name='verify-logout'),
]