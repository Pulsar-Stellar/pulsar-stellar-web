from django.shortcuts import render

def docs_home(request):
    return render(request, 'docs_templates/views/docs_home.html')
