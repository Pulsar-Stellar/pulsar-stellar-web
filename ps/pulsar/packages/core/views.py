from django.shortcuts import render


def core_home(request):
    return render(request, 'core_templates/views/core_home.html')

def core_about(request):
    return render(request, 'core_templates/views/core_about.html', {'title':'About' })

def core_research(request):
    return render(request, 'core_templates/views/core_research.html', {'title':'Research' })
