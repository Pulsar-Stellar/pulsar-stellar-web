from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
    path('admin/', admin.site.urls),
]

core_urls = [
    path('', include('core.urls')),
    path('blog/', include('blog.urls')),
    path('docs/', include('docs.urls')),
]

third_part_urls = [
    path('', include('verify.urls')),
    path('', include('social_django.urls')),
]


urlpatterns += core_urls + third_part_urls

